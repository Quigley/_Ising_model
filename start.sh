#!/bin/bash
#This is the main code used in my program, it will iterate through each of the scripts I created for my assignment. Make sure you have downloaded com0 -> com6 and Func1 and Func2. I hope you had a nice break, I apologise for the messiness of my coding! 


for i in {0..6};
do python com$i.py;
echo press a to go forward, press b to animate the code, and ctrl z to quit the program to view any of the files
read part
t1="a"
t2="b"
t3="c"

if [ "$t1" = "$part" ]; then
   echo Next part
fi

if [ "$t2" = "$part" ]; then
   convert -delay 20 -loop 0 *.pbm temp_range.gif
   display temp_range.gif
fi


done

