
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 21 12:15:11 2016

@author: kevin
"""




import numpy as np
import Func as fu
import get_array
import matplotlib.pyplot as plt	
import imageio	
#inputs
"""
N = input("Enter the width/height of the matrix:- ")
Steps = input("Enter the number of monte carlo steps:- ")
T1 = input("Enter the temperture range beginning")
T2 = input("Enter the temperture range ending")
"""
N,Steps, T1,T2= 100,100,100, 1000
T_range = 10
T = np.linspace(T1,T2, T_range)
#Boltsman constant
Bolt = 8.617*10**-5




#initialize matrix
S = fu.Ini(N)


# set matrix to ones and minus ones
for i in range(N):
	for j in range(N):
		if S[i][j] == 0:
			S[i][j] = -1
		 
#List of types of Boundary conditions
#Periodic Boundary conditions
for i in range(N/2-1):
	S[0][2*i] = 1
for i in range(N/2-2):
	S[0][2*i+1] = -1

for i in range(N/2-1):
	S[N-1][2*i] = 1
for i in range(N/2-2):
	S[N-1][2*i+1] = -1
for i in range(N/2-1):
	S[N-1][2*i] = 1
for i in range(N/2-2):
	S[N-1][2*i+1] = -1

for i in range(N/2-1):
	S[2*i][0] = 1
for i in range(N/2-2):
	S[2*i+1][0] = -1



# save initial array:

get_array.myplot(S, "Ini.pbm")
#2Methods of monte carlo calculations
#Iterations working by counting numerically

#For iterations on multiple tempertures

Thermcheck= np.zeros(Steps)
Equillibrium_steps = np.zeros(T_range)

Energy = np.zeros(T_range)
Magnetization = np.zeros(T_range)
SpecificHeat = np.zeros(T_range)
Susceptibility = np.zeros(T_range)
kk = 0
images =[]
for t in T:
	
	w =0
	z = 0
	S = fu.Ini(N)
	for i in range(N):
		for j in range(N):
			if S[i][j] == 0:
				S[i][j] = -1	
	while z< Steps-1:
	
		Thermcheck= np.zeros(Steps)
		E = M = Ms =Es = 0.0
		for i in range(N-1):
			for j in range(N-1):
				beta = 1/(Bolt*T)
				be = beta[t]
				S = fu.setmonte(S,be,i,j)
		Thermcheck[z] = fu.Energy(S,N)
		w = z
		z+=1
		if z > 0:
			if Thermcheck[z] ==Thermcheck[z-1]:
				z = Steps
				print "Equillibrium reached! %d", z
		
	
	
	
	Equillibrium_steps[kk]= w	
	ene= fu.Energy(S,N)
	mag= fu.Mag(S)

	E = E +ene
	M = M + mag
	Es = Es+ ene*ene
	Ms = Ms + mag*mag
	Energy[kk] = E/(w*N*N)
	Magnetization[kk] = M/(w*N*N)
	SpecificHeat[kk]= (Es/Steps - E*E/(w*w))/(N*t*t)
	Susceptibility[kk]=(Ms/Steps - M*M/(w*w))/(N*t*t)
	
	
	kk+=1
	print kk
	for i in range(N):
		for j in range(N):
			if S[i][j] == -1:
				S[i][j] = 0
	get_array.myplot(S, "%s.pbm" % t)


print Magnetization
print Susceptibility
# set matrix to ones and zeros
for i in range(N):
	for j in range(N):
		if S[i][j] == -1:
			S[i][j] = 0

#To turn the range of pbms created into a gif




