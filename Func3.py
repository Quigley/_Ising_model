# -*- coding: utf-8 -*-
"""
Created on Wed Dec 21 12:15:11 2016

@author: kevin
"""

import numpy as np
import random


#This randomly selects elements of the matrix to update spin
def ranmonte(spin,beta):
	C = spin.shape
	n = C[0]
	m = C[1]
	a = np.random.randint(1,n-1)
	b = np.random.randint(1,n-1)
	spin =flipspin(spin,a,b,beta)
	return spin
#This iterates through selected elements of the matrix 

def setmonte(spin, beta,a,b):
	spin = flipspin(spin,a,b,beta)
	return spin

def Energy(spin, N):
	energy = 0
	for i in range(N-1):
		for j in range(N-1):
			S = spin[i,j]
			nb = spin[i,j+1]+spin[i,j-1]+spin[i+1,j]+spin[i-1,j]
			energy+= -nb*S
	print energy
	return energy/4


def Mag(spin):
    mag = np.sum(spin)
    return mag

def randomize(A):
	c = A.shape 
	q = np.prod(c) 
	spin =np.rint(q).reshape(c)
	return spin

def flipspin(spin,a,b,beta):
	

	s = spin[a,b]
	ss = spin[a+1,b]+spin[a-1,b]+spin[a,b+1]+spin[a,b-1]
	dE = 2*s*ss
	if dE < 0:
		s*= -1
	elif random.random() < np.exp(-dE*beta):
	 #I tried to change the number generator to be maxwellian, but I had untold trouble trying to do this!
			
		s*= -1
	spin[a,b] =s
	return spin

def up(A):
	c = A.shape 
	q = np.prod(c) 
	spin =np.ones(q).reshape(c)
	return spin

def loadmatrix(f):
	spin = np.loadtxt('f')
	
	return spin




def Ini(N):
	
	
	A=np.random.random_sample(N*N).reshape(N,N)
	B=np.rint(A)
	return B



