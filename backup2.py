
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 21 12:15:11 2016

@author: kevin
"""




import numpy as np
import Func as fu
import get_array
import matplotlib.pyplot as plt		
#inputs
"""
N = input("Enter the width/height of the matrix:- ")
Steps = input("Enter the number of monte carlo steps:- ")
T1 = input("Enter the temperture range beginning")
T2 = input("Enter the temperture range ending")
"""
N,Steps, T1,T2= 100,100,80, 1000
T = np.linspace(T1,T2, 100)
#Boltsman constant
Bolt = 8.617*10**-5
beta = 1/(Bolt*T)
Energy = np.zeros(Steps)
Magnetization = np.zeros(Steps)
SpecificHeat = np.zeros(Steps)
Susceptibility = np.zeros(Steps)

#initialize matrix
S = fu.Ini(N)


# set matrix to ones and minus ones
for i in range(N):
	for j in range(N):
		if S[i][j] == 0:
			S[i][j] = -1
		 
#List of types of Boundary conditions
#Periodic Boundary conditions
for i in range(N/2-1):
	S[0][2*i] = 1
for i in range(N/2-2):
	S[0][2*i+1] = -1

for i in range(N/2-1):
	S[N-1][2*i] = 1
for i in range(N/2-2):
	S[N-1][2*i+1] = -1
for i in range(N/2-1):
	S[N-1][2*i] = 1
for i in range(N/2-2):
	S[N-1][2*i+1] = -1

for i in range(N/2-1):
	S[2*i][0] = 1
for i in range(N/2-2):
	S[2*i+1][0] = -1



# save initial array:

get_array.myplot(S, "Ini.pbm")
#2Methods of monte carlo calculations
#Iterations working by counting numerically


for z in range(Steps): 
	E = M = Ms =Es = 0.0
	for i in range(N-1):
		for j in range(N-1):
			s = fu.setmonte(S,beta,i,j)

	ene= fu.Energy(S,N)
	mag= fu.Mag(S)
	
	E = E +ene
	M = M + mag
	Es = Es+ ene*ene
	Ms = Ms + mag*mag
	Energy[z] = E/(Steps*N*N)
	Magnetization[z] = M/(Steps*N*N)
	SpecificHeat[z]= (Es/Steps - E*E/(Steps*Steps))/(N*T*T)
	Susceptibility[z]=(Ms/Steps - M*M/(Steps*Steps))/(N*T*T)

print Magnetization
# set matrix to ones and zeros
for i in range(N):
	for j in range(N):
		if S[i][j] == -1:
			S[i][j] = 0


 
get_array.myplot(S, "Fin.pbm")
