import matplotlib.pyplot as plt



import numpy as np
import matplotlib.pyplot as plt

x = [0.01,0.0025,0.0011,0.00082,0.00063,0.0005]
y = [2.49,2.35,2.32,2.315,2.306,2.304] 

fit = np.polyfit(x,y,1)
fit_fn = np.poly1d(fit) 
# fit_fn is now a function which takes in x and returns an estimate for y
print "Our estimate for the critical temperature is:"
print fit_fn(0)
plt.plot(x,y, 'yo', x, fit_fn(x), '--k')
plt.xlim(0, 0.011)
plt.ylim(2.29, 2.50)
plt.xlabel("Temperture J/KB")
plt.ylabel("1/N^2")
plt.show()
