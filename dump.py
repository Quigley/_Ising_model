
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 21 12:15:11 2016

@author: kevin
"""
		
#1 List of types of Boundary conditions
# BC where all up





#2Methods of monte carlo calculations
#monte carlo iterating by counting numerically 0,0, -> 0,1 _>
for z in range(Steps): 
	for i in range(N-1):
		for j in range(N-1):
			s = fu.setmonte(S,beta,i,j)
	

#monte carlo iterating for random points
for i in range(Steps):
	S= fu.ranmonte(S,beta)
 




#Methods of counting over different tempertures
#For iterations of a single temperture 
for z in range(Steps): 
	E = M = Ms =Es = 0.0
	for i in range(N-1):
		for j in range(N-1):
			s = fu.setmonte(S,beta,i,j)

	ene= fu.Energy(S,N)
	mag= fu.Mag(S)
	
	E = E +ene
	M = M + mag
	Es = Es+ ene*ene
	Ms = Ms + mag*mag
	Energy[z] = E/(Steps*N*N)
	Magnetization[z] = M/(Steps*N*N)
	SpecificHeat[z]= (Es/Steps - E*E/(Steps*Steps))/(N*T*T)
	Susceptibility[z]=(Ms/Steps - M*M/(Steps*Steps))/(N*T*T)
	
