
# -*- coding: utf-8 -*-
# The purpose of this code is to find how many steps are neccessary for the energy of the system to reach an energy equillibrium
"""
Created on Wed Dec 21 12:15:11 2016

@author: kevin
"""




import numpy as np
import Func2 as fu
import get_array
import matplotlib.pyplot as plt	
import imageio	
print "The purpose of this code is to find how many steps are neccessary for the energy of the system to reach an energy equillibrium"
#inputs
"""
N = input("Enter the width/height of the matrix:- ")
Steps = input("Enter the number of monte carlo steps:- ")
T1 = input("Enter the temperture range beginning")
T2 = input("Enter the temperture range ending")
"""
N,Steps, T1,T2= 100,100,100, 1000
T_range = 10
T = np.linspace(T1,T2, T_range)
Tw = np.asarray(T)
print Tw[0]
#Boltsman constant
Bolt = 8.617*10**-5




#initialize matrix
S = fu.Ini(N)


# set matrix to ones and minus ones
for i in range(N):
	for j in range(N):
		if S[i][j] == 0:
			S[i][j] = -1
		 
#List of types of Boundary conditions
#Periodic Boundary conditions
fu.periodic11(S,N)




# save initial array:

get_array.myplot(S, "Ini.pbm")
#2Methods of monte carlo calculations
#Iterations working by counting numerically

#For iterations on multiple tempertures

Thermcheck= np.zeros(Steps)
Equillibrium_steps = np.zeros(T_range)

Energy = np.zeros(T_range)
Magnetization = np.zeros(T_range)
SpecificHeat = np.zeros(T_range)
Susceptibility = np.zeros(T_range)
kk = 0
images =[]
t = 0
ste = []
for t in range(T_range):
	
	w =0
	z = 0
	S = fu.Ini(N)
	Thermcheck= np.zeros(Steps)
	for i in range(N):
		for j in range(N):
			if S[i][j] == 0:
				S[i][j] = -1	
	while z< Steps-1:
	
		i = 0
		j = 0
		E = M = Ms =Es = 0.0
		for i in  range(N-1):
			for j in  range(N-1):
				beta = 1/(Bolt*T)
				be = beta[t]
				if i !=0 and j!=0:
					S = fu.setmonte(S,be,i,j)
				
				
		Thermcheck[z] = fu.Energy(S,N)
		
		w = z
		
		if z > 0:
			if Thermcheck[z]-1.0 <Thermcheck[z-1]< Thermcheck[z]+1.0:
				
				print "Equillibrium reached after ", w, "steps"
				
				z = Steps-1
				ste.append(w)
		z+=1


		
	
	
	Equillibrium_steps[kk]= w	
	ene= fu.Energy(S,N)
	mag= fu.Mag(S)

	E = E +ene
	M = M + mag
	Es = Es+ ene*ene
	Ms = Ms + mag*mag
	Energy[kk] = E/(w*N*N)
	Magnetization[kk] = M/(w*N*N)
	
	

	SpecificHeat[kk]= (Es/Steps - E*E/(w*w))/(N*T[t]*T[t])
	Susceptibility[kk]=(Ms/Steps - M*M/(w*w))/(N*T[t]*T[t])
	# magnetization plot
	
	
	kk+=1
	print "step number",kk
	for i in range(N):
		for j in range(N):
			if S[i][j] == -1:
				S[i][j] = 0
	get_array.myplot(S, "%s.pbm" % t)


print Magnetization
print Susceptibility
# set matrix to ones and zeros
for i in range(N):
	for j in range(N):
		if S[i][j] == -1:
			S[i][j] = 0

#To turn the range of pbms created into a gif

print ste


