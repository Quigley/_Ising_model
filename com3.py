
# -*- coding: utf-8 -*-
# The purpose of this code is to produce diagrams of the behaviours of different thermodynamic processes for a given range of temperture.
"""
Created on Wed Dec 21 12:15:11 2016

@author: kevin
"""




import numpy as np
import Func2 as fu
import get_array
import matplotlib.pyplot as plt	
import imageio	
#inputs
print " The purpose of this code is to produce diagrams of the behaviours of different thermodynamic processes for a given range of temperture. In order to show the behaviour of the properties, I would reccomend the following:- \n N,Steps, T1,T2, T_range = 16 ,1000, 1, 5, 200"

N = input("Enter the width/height of the matrix:- ")
Steps = input("Enter the number of monte carlo steps:- ")
T1 = input("Enter the temperture range beginning in Kb:- ")
T2 = input("Enter the temperture range ending in Kb:- ")
T_range = input("Enter the number of points on the diagram:- ")


T1= T1*(1/(8.617*10**-5))
T2 = T2*(1/(8.617*10**-5))
T = np.linspace(T1,T2, T_range)
Tw = np.asarray(T)

print Tw[0]

#Boltsman constant
Bolt = 8.617*10**-5




#initialize matrix
S = fu.Ini(N)


# set matrix to ones and minus ones
for i in range(N):
	for j in range(N):
		if S[i][j] == 0:
			S[i][j] = -1
		 
#List of types of Boundary conditions
#Periodic Boundary conditions
fu.periodic11(S,N)




# save initial array:

get_array.myplot(S, "Ini.pbm")


#For iterations on multiple tempertures

Thermcheck= np.zeros(Steps)
Equillibrium_steps = np.zeros(T_range)

Energy = np.zeros(T_range)
Magnetization = np.zeros(T_range)
SpecificHeat = np.zeros(T_range)
Susceptibility = np.zeros(T_range)
kk = 0
images =[]
t = 0
ste = []

#main code
for t in range(T_range):
	E=M=Es=Ms =0
	w =0
	z = 0
	S = fu.Ini(N)
	#convert to 1, -1
	for i in range(N):
		for j in range(N):
			if S[i][j] == 0:
				S[i][j] = -1	
	# to Initialize the matrix to be within a given range
	while z< Steps-1:
	
		i = 0
		j = 0
		z+=1
		E = M = Ms =Es = 0.0
		for i in  range(N-1):
			for j in  range(N-1):
				beta = 1/(Bolt*T)
				be = beta[t]
				if i !=0 and j!=0:
					S = fu.setmonte(S,be,i,j)
		
	z=0
	E = M = Ms =Es = 0.0
	#To show the devolopement of he properties
	while z< Steps-1:
	
		i = 0
		j = 0
		
		z+=1
		for i in  range(N-1):
			for j in  range(N-1):

				beta = 1/(Bolt*T)
				be = beta[t]
				if i !=0 and j!=0:
					S = fu.setmonte(S,be,i,j)
		
		ene= fu.Energy(S,N)
		mag= fu.Mag(S)

		E = E +ene
		M = M + mag
		Es = Es+ ene*ene
		Ms = Ms + mag*mag
		Energy[t] = E/((Steps)*N*N)
		Magnetization[t] = M/((Steps)*N*N)

		SpecificHeat[t]= (Es/Steps - E*E/((Steps)*(Steps)))/(N*1/(beta[t]*beta[t]))

		Susceptibility[t]=(Ms/Steps - M*M/((Steps)*(Steps)))/(N*1/(beta[t]*beta[t]))

	# magnetization plot
	
	
	kk+=1
	print kk
	for i in range(N):
		for j in range(N):
			if S[i][j] == -1:
				S[i][j] = 0
	"""get_array.myplot(S, "%s.pbm" % t)"""
T = 1/beta
print T

# plot the energy, Magnetization, Specific heat and magnetic susceptibility 



plt.plot(T, Energy, 'o', color="blue", label=' Energy');
plt.xlabel("Temperature (T)", fontsize=20);
plt.ylabel("Energy ", fontsize=20);
plt.show()

plt.plot(T, abs(Magnetization), 'o', color="blue", label='Magnetization');
plt.xlabel("Temperature (T)", fontsize=20);
plt.ylabel("Magnetization ", fontsize=20);
plt.show()

plt.plot(T, SpecificHeat, 'o', color="blue", label='Specific Heat');
plt.xlabel("Temperature (T)", fontsize=20);
plt.ylabel("Specific Heat ", fontsize=20);
plt.show()


plt.plot(T, Susceptibility, 'o', color="blue", label='Specific Heat');
plt.xlabel("Temperature (T)", fontsize=20);
plt.ylabel("Susceptibility", fontsize=20);
plt.show()




