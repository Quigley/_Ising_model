
# -*- coding: utf-8 -*-

# The aim of this code is to find different thermal equillibrium for given tempereratures!

"""
Created on Wed Dec 21 12:15:11 2016

@author: kevin
"""




import numpy as np
import Func as fu
import get_array
import matplotlib.pyplot as plt	
import imageio	
print " The aim of this code is to find different thermal equillibrium for given tempereratures! In order to demonstrate the flip of the majority charge for tempertures close to the critical temperture, I would recomend the following inputs:- \n N, Steps, T = 16, 1000, 2.2"

#inputs

N = input("Enter the width/height of the matrix:- ")
Steps = input("Enter the number of monte carlo steps:- ")
T = input("Enter the Temperture in inverse boltzman constants, J/KB:-")

T = T*1/(8.617*10**-5)

#Boltsman constant
Bolt = 8.617*10**-5




#initialize matrix
S = fu.Ini(N)
get_array.myplot(S, "Ini.pbm")

# set matrix to ones and minus ones
for i in range(N):
	for j in range(N):
		if S[i][j] == 0:
			S[i][j] = -1
print S
		 
#List of types of Boundary conditions
#Periodic Boundary conditions
for i in range(N/2-1):
	S[0][2*i] = 1
for i in range(N/2-2):
	S[0][2*i+1] = -1

for i in range(N/2-1):
	S[N-1][2*i] = 1
for i in range(N/2-2):
	S[N-1][2*i+1] = -1
for i in range(N/2-1):
	S[N-1][2*i] = 1
for i in range(N/2-2):
	S[N-1][2*i+1] = -1

for i in range(N/2-1):
	S[2*i][0] = 1
for i in range(N/2-2):
	S[2*i+1][0] = -1



# save initial array:


#2Methods of monte carlo calculations
#Iterations working by counting numerically

#For iterations on multiple tempertures
beta = 1/(Bolt*T)
Energy = np.zeros(Steps)
Magnetization = np.zeros(Steps)
SpecificHeat = np.zeros(Steps)
Susceptibility = np.zeros(Steps)


for z in range(Steps): 
	E = M = Ms =Es = 0.0
	for i in range(N-1):
		for j in range(N-1):
			if i!=0 and j!=0:
				s = fu.setmonte(S,beta,i,j)

	mag= fu.Mag(S)
	
	M = mag
	

	Magnetization[z] = M/((N-2)*(N-2))
	
	print z




plt.plot(Magnetization)
plt.show()

# set matrix to ones and zeros
for i in range(N):
	for j in range(N):
		if S[i][j] == -1:
			S[i][j] = 0




